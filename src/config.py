# Part of Carburetor project
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020-2024.

"""
main configurations for carburetor
"""

import gettext
import os

import pycountry
from gi.repository import Gio


def _nodes():
    """
    return sorted list of exit nodes
    """
    codes = ("CA", "CH", "DE", "ES", "GB", "IT", "FR", "NL", "SE", "US")
    countries = [pycountry.countries.get(alpha_2=code) for code in codes]
    language = os.environ.get("LANG", "en_US.UTF-8").split(".")[0]
    try:
        countries_l10n = gettext.translation(
            "iso3166-1", pycountry.LOCALES_DIR, [language]
        )
        return {
            country: f"{countries_l10n.gettext(country.name)} {country.flag}"
            for country in countries
        }
    except FileNotFoundError:
        # If pycountry does not provide country translations for the language
        return {
            country: f"{country.name} {country.flag}" for country in countries
        }


COMMAND = "tractor"
DCONF = Gio.Settings.new("org.tractor")
NODES = _nodes()
