# Carburetor: Anonymous Browsing Made Easy
Experience seamless anonymous browsing with Carburetor - your privacy shield across devices. Built for GNU/Linux with GNOME integration and powered by Tor network.

![Main window of Carburetor](https://tractor.frama.io/images/carburetor-main.png)

**Key Features:**
- 📱 **Cross-Platform Privacy**: Works seamlessly on phones and desktops
- 🛡️ **Tor Network Integration**: Automatic encryption and IP masking
- 🎯 **GNOME-First Design**: Native look and feel for GTK environments
- 🚀 **One-Click Security**: Connect to Tor with a single button
- 🔧 **Smart Configuration**: Automatic optimal setup with manual override
- 📦 **Distro-Ready**: Available in major package repositories

## Why Carburetor?
In an era of increasing surveillance, Carburetor provides:
- **Complete Anonymity**: Route traffic through multiple Tor nodes
- **Zero Configuration**: Automatic setup and optimal defaults
- **Transparent Freedom**: Fully Free Software (GPLv3 licensed)
- **Resource Efficiency**: Lightweight compared to browser-based solutions

## Installation
### 📦 Package Managers
Carburetor is natively packaged for many distributions including:

[![Packaging status](https://repology.org/badge/vertical-allrepos/carburetor.svg)](https://repology.org/project/carburetor/versions)

If your distribution isn’t listed, we encourage creating new build recipes.

### 📥 Flatpak
Install via [Flathub](https://flathub.org/apps/io.frama.tractor.carburetor):

```bash
$ flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
$ flatpak install flathub io.frama.tractor.carburetor
```

### 🛠️ From Source
You can clone the Carburetor source in [GNOME Builder](https://apps.gnome.org/app/org.gnome.Builder) and hit `Install`. This will build and install all dependencies as well.

Alternatively, you can manually build and install only Carburetor itself:
```bash
$ git clone https://framagit.org/tractor/carburetor.git
$ cd carburetor
$ meson setup --prefix=/usr build-dir
$ meson install -C build-dir
```

## Getting Started
### Launch Options
**GUI**: Find Carburetor in your application menu

**CLI**:
```bash
carburetor  # Native install
flatpak run io.frama.tractor.carburetor  # Flatpak
```
**GNOME Builder**: Hit `Run Project (shift+ctrl+space)`

### First Run
1. *(optional)* Enable `Set Proxy` from menu for desktop-wide proxy
1. Click `Connect` in the main window
1. Wait for Tor circuit establishment

## Community & Contribution
### Code of Conduct
We adhere to the [GNOME Code of Conduct](https://conduct.gnome.org) to maintain a welcoming environment. All participants must:
- 🤝 Use inclusive language
- 🧠 Assume good faith
- 🔍 Search before asking
- 📣 Disagree constructively

### How to Help
|Area          |How to Start|
|--------------|------------|
|🐛 Bug Reports|[File issues](https://framagit.org/tractor/carburetor/-/blob/main/CONTRIBUTING.md#issues)|
|💻 Development|Check open [help-wanted](https://framagit.org/tractor/carburetor/-/issues/?label_name%5B%5D=help-wanted) issues|
|🌍 Translation|Localize via [Weblate](https://hosted.weblate.org/engage/carburetor)|
|📖 Docs	   |Improve help articles or man pages|

Full contribution guide: [CONTRIBUTING.md](https://framagit.org/tractor/carburetor/-/blob/main/CONTRIBUTING.md)

## Free Software Philosophy
Carburetor isn't just a tool - it's a statement. By using and contributing to this GPLv3-licensed project, you:
- 🔓 Reject surveillance capitalism
- 🛠️ Gain full control over your digital tools
- 🌱 Support the Free Software ecosystem
- 📚 Learn from transparent code

> Privacy is not a feature, but a fundamental right.
>
> – Danial Behzadi, Project Founder
