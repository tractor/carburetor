<?xml version='1.0' encoding='UTF-8'?>
<!-- Created with Cambalache 0.94.1 -->
<!-- Danial Behzadi <dani.behzi@ubuntu.com>, 2022-2024 -->
<interface domain="carburetor">
  <!-- interface-name preferences.ui -->
  <!-- interface-description This file is part of Carburetor. -->
  <!-- interface-copyright Carburetor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Carburetor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Carburetor.  If not, see <http://www.gnu.org/licenses/>. -->
  <!-- interface-authors Danial Behzadi <dani.behzi@ubuntu.com> -->
  <requires lib="gtk" version="4.12"/>
  <requires lib="libadwaita" version="1.6"/>
  <object class="AdwPreferencesDialog" id="PreferencesDialog">
    <child>
      <object class="AdwPreferencesPage" id="GeneralPreferencesPage">
        <property name="icon-name">applications-system-symbolic</property>
        <property name="title" translatable="yes" context="PreferencesPage title">General</property>
        <child>
          <object class="AdwPreferencesGroup" id="ExitNodeGroup">
            <property name="title" translatable="yes" context="PreferencesGroup title">Exit Node</property>
            <child>
              <object class="AdwComboRow" id="ExitCountryCombo">
                <property name="factory">ExitNodeFactory</property>
                <property name="subtitle" translatable="yes">The country you want to connect from</property>
                <property name="title" translatable="yes">Exit Country</property>
                <signal name="realize" handler="on_exitcountry_realize"/>
                <signal name="notify::selected-item" handler="on_exitcountry_change"/>
              </object>
            </child>
            <child>
              <object class="AdwEntryRow" id="CountryCode">
                <property name="input-hints">lowercase|no-emoji|no-spellcheck</property>
                <property name="input-purpose">alpha</property>
                <property name="sensitive">False</property>
                <property name="title" translatable="yes">Country Code</property>
                <signal name="realize" handler="on_country_code_realize"/>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="AdwPreferencesGroup" id="AcceptConnectionGroup">
            <property name="title" translatable="yes" context="PreferencesGroup title">Connections</property>
            <child>
              <object class="AdwSwitchRow">
                <property name="subtitle" translatable="yes">Allow external devices to use this network</property>
                <property name="title" translatable="yes">Accept Incoming Connections</property>
                <signal name="realize" handler="on_actionacceptconnection_realize"/>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow">
                <property name="subtitle" translatable="yes">Restrict connections to ports 80 and 443</property>
                <property name="title" translatable="yes">Enable Fascist Firewall Mode</property>
                <signal name="realize" handler="on_fascist_realize"/>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="AdwPreferencesPage" id="PortsPreferencesPage">
        <property name="icon-name">preferences-system-network-proxy-symbolic</property>
        <property name="title" translatable="yes" context="PreferencesPage title">Proxy</property>
        <child>
          <object class="AdwPreferencesGroup" id="PortsGroup">
            <property name="description" translatable="yes">Ports on which the application listens for incoming connections</property>
            <property name="title" translatable="yes">Listening Ports</property>
            <child>
              <object class="AdwSpinRow" id="SocksRow">
                <property name="adjustment">SocksPortAdjustment</property>
                <property name="subtitle" translatable="yes" context="Preferences SpinRow subtitle">Main connection point for secure communication</property>
                <property name="title" translatable="yes" context="Preferences SpinRow title">SOCKS</property>
                <signal name="realize" handler="on_actionsocksport_realize"/>
              </object>
            </child>
            <child>
              <object class="AdwSpinRow" id="DNSRow">
                <property name="adjustment">DNSPortAdjustment</property>
                <property name="subtitle" translatable="yes" context="Preferences SpinRow subtitle">A local DNS server for enhanced privacy</property>
                <property name="title" translatable="yes" context="Preferences SpinRow title">DNS</property>
                <signal name="realize" handler="on_actiondnsport_realize"/>
              </object>
            </child>
            <child>
              <object class="AdwSpinRow" id="HTTPRow">
                <property name="adjustment">HTTPPortAdjustment</property>
                <property name="subtitle" translatable="yes" context="Preferences SpinRow subtitle">A fallback HTTP Tunnel for simple connections</property>
                <property name="title" translatable="yes" context="Preferences SpinRow title">HTTP</property>
                <signal name="realize" handler="on_actionhttpport_realize"/>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="AdwPreferencesPage" id="BridgesPreferencesPage">
        <property name="icon-name">network-wired-no-route-symbolic</property>
        <property name="title" translatable="yes" context="PreferencesPage title">Bridges</property>
        <child>
          <object class="AdwPreferencesGroup" id="BridgeTypeGroup">
            <property name="title" translatable="yes" context="PreferencesGroup title">Pluggable Transports</property>
            <child>
              <object class="AdwComboRow" id="BridgeTypeCombo">
                <property name="factory">BridgeTypeFactory</property>
                <property name="title" translatable="yes">Type of Transport</property>
                <signal name="realize" handler="on_bridgetypecombo_realize"/>
                <signal name="notify::selected-item" handler="on_bridgetype_change"/>
              </object>
            </child>
            <child>
              <object class="AdwActionRow" id="PluginRow">
                <property name="title" translatable="yes">Transport Executable File</property>
                <signal name="realize" handler="on_pluginrow_realize"/>
                <child>
                  <object class="GtkButton" id="PluginButton">
                    <property name="icon-name">document-open-symbolic</property>
                    <property name="label" translatable="yes">None</property>
                    <property name="valign">center</property>
                    <signal name="clicked" handler="on_pluginbutton_clicked"/>
                    <signal name="realize" handler="on_pluginbutton_realize"/>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="AdwPreferencesGroup" id="BridgesGroups">
            <property name="description" translatable="yes">Bridges help you securely access the Tor Network in places where Tor is blocked</property>
            <property name="title" translatable="yes" context="PreferencesGroup title">Bridges</property>
            <signal name="realize" handler="on_bridges_realize"/>
            <child>
              <object class="AdwActionRow">
                <property name="subtitle">🏸 🦦 🐢 📕  217.94.24.184:9001</property>
                <property name="subtitle-lines">1</property>
                <property name="title">Vanilla</property>
                <child>
                  <object class="GtkButton">
                    <property name="css-classes">destructive-action
circular</property>
                    <property name="icon-name">list-remove-symbolic</property>
                    <property name="valign">center</property>
                  </object>
                </child>
              </object>
            </child>
            <child>
              <object class="AdwButtonRow">
                <property name="action-name">open-add</property>
                <property name="start-icon-name">list-add-symbolic</property>
                <property name="title" translatable="yes">_Add</property>
                <property name="use-underline">True</property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="AdwPreferencesGroup">
            <child>
              <object class="AdwActionRow">
                <property name="action-name">app.open_externally</property>
                <property name="activatable">True</property>
                <property name="subtitle" translatable="yes">View as a text file to edit and share bridges</property>
                <property name="title" translatable="yes">_Open Externally</property>
                <property name="use-underline">True</property>
                <child>
                  <object class="GtkImage">
                    <property name="icon-name">text-editor-symbolic</property>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="AdwPreferencesGroup">
            <property name="description" translatable="yes">Since many bridge addresses aren’t public, you may need to request some from the Tor Project</property>
            <property name="title" translatable="yes" context="PreferencesGroup title">Find More Bridges</property>
            <child>
              <object class="AdwActionRow">
                <property name="action-name">app.bridgedb</property>
                <property name="activatable">True</property>
                <property name="subtitle" translatable="yes">Visit BridgeDB website</property>
                <property name="title" translatable="yes">Web</property>
                <child>
                  <object class="GtkImage">
                    <property name="icon-name">web-browser-symbolic</property>
                  </object>
                </child>
              </object>
            </child>
            <child>
              <object class="AdwActionRow">
                <property name="action-name">app.email</property>
                <property name="activatable">True</property>
                <property name="subtitle" translatable="yes">Exclusively from Gmail or Riseup</property>
                <property name="title" translatable="yes">Email</property>
                <child>
                  <object class="GtkImage">
                    <property name="icon-name">mail-send-symbolic</property>
                  </object>
                </child>
              </object>
            </child>
            <child>
              <object class="AdwActionRow">
                <property name="action-name">app.telegram</property>
                <property name="activatable">True</property>
                <property name="subtitle">Message GetBridgesBot</property>
                <property name="title" translatable="yes">Telegram</property>
                <child>
                  <object class="GtkImage">
                    <property name="icon-name">chat-message-new-symbolic</property>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
  </object>
  <object class="GtkSignalListItemFactory" id="ExitNodeFactory">
    <signal name="setup" handler="on_factory_setup"/>
    <signal name="bind" handler="on_factory_bind"/>
  </object>
  <object class="GtkAdjustment" id="SocksPortAdjustment">
    <property name="lower">1.0</property>
    <property name="step-increment">1.0</property>
    <property name="upper">65535.0</property>
  </object>
  <object class="GtkAdjustment" id="DNSPortAdjustment">
    <property name="lower">1.0</property>
    <property name="step-increment">1.0</property>
    <property name="upper">65535.0</property>
  </object>
  <object class="GtkAdjustment" id="HTTPPortAdjustment">
    <property name="lower">1.0</property>
    <property name="step-increment">1.0</property>
    <property name="upper">65535.0</property>
  </object>
  <object class="GtkSignalListItemFactory" id="BridgeTypeFactory">
    <signal name="setup" handler="on_bt_factory_setup"/>
    <signal name="bind" handler="on_bt_factory_bind"/>
  </object>
  <object class="GtkFileDialog" id="PluginChooser"/>
</interface>
