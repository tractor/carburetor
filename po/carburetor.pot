# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the carburetor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: carburetor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-27 13:49+0330\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/applications/io.frama.tractor.carburetor.desktop.in:5
#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:5
#: data/gtk/main.ui:47
msgid "Carburetor"
msgstr ""

#: data/applications/io.frama.tractor.carburetor.desktop.in:6
#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:6
msgid "Browse anonymously"
msgstr ""

#: data/applications/io.frama.tractor.carburetor.desktop.in:12
msgid "Tor;Tractor;Carburetor;"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:9
msgid ""
"Discover anonymous browsing with Carburetor on your phones and computers. "
"Tailored for GNOME, it's your hidden superhero for online adventures, "
"powered by TOR. Carburetor provides a local TOR proxy that hides your IP, "
"ensuring your Internet activities remain encrypted, anonymized, and "
"untraceable. Don't get your hands dirty with system files anymore – just tap "
"into the app, keeping your online world safe and private. Customize settings "
"if you want, but you don't have to. Carburetor is Free Software and puts you "
"in control. No worries, just enjoy your anonymous browsing!"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:17
msgid "Tor"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:18
msgid "Tractor"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:41
msgid "Tractor Team"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:45
msgid "Main Page of Carburetor in Desktop mode"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:50
msgid "Carburetor preferences in dark mode"
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:70
msgid "Updated to the GNOME 47 platform."
msgstr ""

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:71
msgid "Updated translations."
msgstr ""

#: data/gtk/help-overlay.ui:28
msgctxt "ShortcutsGroup title"
msgid "General"
msgstr ""

#: data/gtk/help-overlay.ui:32
msgctxt "shortcut"
msgid "Preferences"
msgstr ""

#: data/gtk/help-overlay.ui:38
msgctxt "shortcut"
msgid "Show Shortcuts"
msgstr ""

#: data/gtk/help-overlay.ui:44
msgctxt "shortcut"
msgid "Quit"
msgstr ""

#: data/gtk/help-overlay.ui:51
msgctxt "ShortcutsGroup title"
msgid "Functionality"
msgstr ""

#: data/gtk/help-overlay.ui:55
msgctxt "shortcut"
msgid "Connect / Disconnect"
msgstr ""

#: data/gtk/help-overlay.ui:61
msgctxt "shortcut"
msgid "New ID"
msgstr ""

#: data/gtk/help-overlay.ui:68
msgctxt "ShortcutsGroup title"
msgid "Logs"
msgstr ""

#: data/gtk/help-overlay.ui:72
msgctxt "shortcut"
msgid "Clear Logs"
msgstr ""

#: data/gtk/help-overlay.ui:78
msgctxt "shortcut"
msgid "Copy Logs"
msgstr ""

#: data/gtk/logs.ui:32
msgid "Logs"
msgstr ""

#: data/gtk/logs.ui:39
msgctxt "tooltip"
msgid "Clear"
msgstr ""

#: data/gtk/logs.ui:46
msgctxt "tooltip"
msgid "Copy"
msgstr ""

#: data/gtk/main.ui:56
msgctxt "Button tooltip"
msgid "Logs"
msgstr ""

#: data/gtk/main.ui:63
msgctxt "Button tooltip"
msgid "New ID"
msgstr ""

#: data/gtk/main.ui:70
msgctxt "Button tooltip"
msgid "Check connection"
msgstr ""

#: data/gtk/main.ui:80
msgctxt "tooltip"
msgid "Main Menu"
msgstr ""

#: data/gtk/main.ui:95 src/ui.py:123
msgid "_Connect"
msgstr ""

#: data/gtk/main.ui:99 src/ui.py:121
msgid "Press the \"Connect\" button to start"
msgstr ""

#: data/gtk/main.ui:101 src/ui.py:120
msgid "Stopped"
msgstr ""

#: data/gtk/main.ui:120
msgid "_Set Proxy"
msgstr ""

#: data/gtk/main.ui:126
msgctxt "App Menu Item"
msgid "_Preferences"
msgstr ""

#: data/gtk/main.ui:130
msgctxt "App Menu Item"
msgid "_Keyboard Shortcuts"
msgstr ""

#: data/gtk/main.ui:134
msgctxt "App Menu Item"
msgid "_About Carburetor"
msgstr ""

#: data/gtk/preferences.ui:26
msgctxt "PreferencesPage title"
msgid "General"
msgstr ""

#: data/gtk/preferences.ui:29
msgctxt "PreferencesGroup title"
msgid "Exit Node"
msgstr ""

#: data/gtk/preferences.ui:33
msgid "The country you want to connect from"
msgstr ""

#: data/gtk/preferences.ui:34
msgid "Exit Country"
msgstr ""

#: data/gtk/preferences.ui:44
msgid "Country Code"
msgstr ""

#: data/gtk/preferences.ui:52
msgctxt "PreferencesGroup title"
msgid "Connections"
msgstr ""

#: data/gtk/preferences.ui:55
msgid "Allow external devices to use this network"
msgstr ""

#: data/gtk/preferences.ui:56
msgid "Accept Incoming Connections"
msgstr ""

#: data/gtk/preferences.ui:62
msgid "Restrict connections to ports 80 and 443"
msgstr ""

#: data/gtk/preferences.ui:63
msgid "Enable Fascist Firewall Mode"
msgstr ""

#: data/gtk/preferences.ui:74
msgctxt "PreferencesPage title"
msgid "Proxy"
msgstr ""

#: data/gtk/preferences.ui:77
msgid "Ports on which the application listens for incoming connections"
msgstr ""

#: data/gtk/preferences.ui:78
msgid "Listening Ports"
msgstr ""

#: data/gtk/preferences.ui:82
msgctxt "Preferences SpinRow subtitle"
msgid "Main connection point for secure communication"
msgstr ""

#: data/gtk/preferences.ui:83
msgctxt "Preferences SpinRow title"
msgid "SOCKS"
msgstr ""

#: data/gtk/preferences.ui:90
msgctxt "Preferences SpinRow subtitle"
msgid "A local DNS server for enhanced privacy"
msgstr ""

#: data/gtk/preferences.ui:91
msgctxt "Preferences SpinRow title"
msgid "DNS"
msgstr ""

#: data/gtk/preferences.ui:98
msgctxt "Preferences SpinRow subtitle"
msgid "A fallback HTTP Tunnel for simple connections"
msgstr ""

#: data/gtk/preferences.ui:99
msgctxt "Preferences SpinRow title"
msgid "HTTP"
msgstr ""

#: data/gtk/preferences.ui:110
msgctxt "PreferencesPage title"
msgid "Bridges"
msgstr ""

#: data/gtk/preferences.ui:113
msgctxt "PreferencesGroup title"
msgid "Pluggable Transports"
msgstr ""

#: data/gtk/preferences.ui:117
msgid "Type of Transport"
msgstr ""

#: data/gtk/preferences.ui:124
msgid "Transport Executable File"
msgstr ""

#: data/gtk/preferences.ui:129 src/handler.py:244 src/ui.py:226
msgid "None"
msgstr ""

#: data/gtk/preferences.ui:141
msgid ""
"Bridges help you securely access the Tor Network in places where Tor is "
"blocked"
msgstr ""

#: data/gtk/preferences.ui:142
msgctxt "PreferencesGroup title"
msgid "Bridges"
msgstr ""

#: data/gtk/preferences.ui:163 src/handler.py:369
msgid "_Add"
msgstr ""

#: data/gtk/preferences.ui:175
msgid "View as a text file to edit and share bridges"
msgstr ""

#: data/gtk/preferences.ui:176
msgid "_Open Externally"
msgstr ""

#: data/gtk/preferences.ui:189
msgid ""
"Since many bridge addresses aren’t public, you may need to request some from "
"the Tor Project"
msgstr ""

#: data/gtk/preferences.ui:190
msgctxt "PreferencesGroup title"
msgid "Find More Bridges"
msgstr ""

#: data/gtk/preferences.ui:195
msgid "Visit BridgeDB website"
msgstr ""

#: data/gtk/preferences.ui:196
msgid "Web"
msgstr ""

#: data/gtk/preferences.ui:208
msgid "Exclusively from Gmail or Riseup"
msgstr ""

#: data/gtk/preferences.ui:209
msgid "Email"
msgstr ""

#: data/gtk/preferences.ui:222
msgid "Telegram"
msgstr ""

#: src/actions.py:95
msgid "translator-credits"
msgstr ""

#: src/actions.py:117
msgid "_Cancel"
msgstr ""

#: src/actions.py:127
msgid "Disconnecting…"
msgstr ""

#: src/actions.py:131
msgid "Connecting…"
msgstr ""

#: src/actions.py:168
msgid "Text copied to clipboard"
msgstr ""

#: src/actions.py:179
msgid "You have a new identity!"
msgstr ""

#: src/actions.py:181
msgid "Tractor is not running!"
msgstr ""

#: src/actions.py:211
msgid "Proxy has been set"
msgstr ""

#: src/actions.py:214
msgid "Proxy has been unset"
msgstr ""

#: src/actions.py:291
msgid "No relevant bridges found"
msgstr ""

#: src/actions.py:306
msgid "Transport executable not found"
msgstr ""

#: src/actions.py:347
msgid "One of the listener ports is in use"
msgstr ""

#: src/actions.py:383
msgid "Your connection is secure"
msgstr ""

#: src/actions.py:386
msgid "Failed to connect to TOR network"
msgstr ""

#: src/actions.py:394
msgid "Connection check complete"
msgstr ""

#: src/actions.py:398
msgid "Try again"
msgstr ""

#: src/actions.py:399
msgid "Quit"
msgstr ""

#: src/handler.py:105 src/handler.py:140 src/handler.py:159
msgid "Other (Manual)"
msgstr ""

#: src/handler.py:135
msgid "Auto (Best)"
msgstr ""

#: src/handler.py:245 src/handler.py:306
msgid "Vanilla"
msgstr ""

#: src/handler.py:246 src/handler.py:315
msgid "Obfuscated"
msgstr ""

#: src/handler.py:247 src/handler.py:317
msgid "Snowflake"
msgstr ""

#: src/handler.py:248 src/handler.py:319
msgid "Conjure"
msgstr ""

#: src/handler.py:328
msgid "Connected"
msgstr ""

#: src/handler.py:347
msgid "Remove"
msgstr ""

#: src/handler.py:418
msgid "Duplicate"
msgstr ""

#: src/ui.py:146
msgid "Running"
msgstr ""

#: src/ui.py:147
msgid "Socks Port"
msgstr ""

#: src/ui.py:148
msgid "DNS Port"
msgstr ""

#: src/ui.py:149
msgid "HTTP Port"
msgstr ""

#: src/ui.py:160
msgid "_Disconnect"
msgstr ""

#: src/ui.py:178
msgid "Carburetor is connected"
msgstr ""

#: src/ui.py:180
msgid "Proxy has been set too"
msgstr ""

#: src/ui.py:183
msgid "You may want to use the ports or toggle the proxy now"
msgstr ""

#: src/ui.py:185
msgid "Toggle Proxy"
msgstr ""
