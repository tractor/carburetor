# Carburetor Translation Project
# Copyright (C) 2018 Tractor Team
# Danial Behzadi <dani.behzi@ubuntu.com>, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025.
# Weblate Translation Memory <noreply-mt-weblate-translation-memory@weblate.org>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: Carburetor\n"
"Report-Msgid-Bugs-To: dani.behzi@ubuntu.com\n"
"POT-Creation-Date: 2025-01-27 13:49+0330\n"
"PO-Revision-Date: 2025-01-27 11:38+0000\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/carburetor/"
"translations/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.10-dev\n"
"Generated-By: pygettext.py 1.5\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: data/applications/io.frama.tractor.carburetor.desktop.in:5
#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:5
#: data/gtk/main.ui:47
msgid "Carburetor"
msgstr "کاربراتور"

#: data/applications/io.frama.tractor.carburetor.desktop.in:6
#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:6
msgid "Browse anonymously"
msgstr "مرور ناشناس"

#: data/applications/io.frama.tractor.carburetor.desktop.in:12
msgid "Tor;Tractor;Carburetor;"
msgstr "Tor;Tractor;Carburetor;تور;تراکتور;کاربراتور;"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:9
msgid ""
"Discover anonymous browsing with Carburetor on your phones and computers. "
"Tailored for GNOME, it's your hidden superhero for online adventures, "
"powered by TOR. Carburetor provides a local TOR proxy that hides your IP, "
"ensuring your Internet activities remain encrypted, anonymized, and "
"untraceable. Don't get your hands dirty with system files anymore – just tap "
"into the app, keeping your online world safe and private. Customize settings "
"if you want, but you don't have to. Carburetor is Free Software and puts you "
"in control. No worries, just enjoy your anonymous browsing!"
msgstr ""
"کشف مرور ناشناس روی تلفن‌ها و رایانه‌ها. کاربراتور که برای گنوم طرّاحی شده، "
"ابرقهرمان پنهانتان برای ماجراجویی‌های برخط است که از تور نیرو می‌گیرد. این "
"برنامه پیشکاری محلی برای تور فراهم کرده که آی‌پیتان را نهفته و فعّالیت‌های "
"اینترنتیتان را رمز شده، ناشناس و غیر قابل ردیابی می‌کد. دیگر نیازی به سر و "
"کله زدن با پرونده‌های سامانه‌ای نیست – با یک اشاره در کاره، دنیای برخطتان امن "
"و محرمانه خواهد شد. تنظیمات قابل شخصی‌سازی هستند، ولی اجباری وجود ندارد. "
"کاربراتور نرم‌افزار آزاد بوده و شما را پشت فرمان قرار می‌دهد. بدون نگرانی، از "
"مرور ناشناستان لذّت ببرید!"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:17
msgid "Tor"
msgstr "تور"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:18
msgid "Tractor"
msgstr "تراکتور"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:41
msgid "Tractor Team"
msgstr "گروه تراکتور"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:45
msgid "Main Page of Carburetor in Desktop mode"
msgstr "صفحهٔ اصلی کاربراتور در حالت میزکار"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:50
msgid "Carburetor preferences in dark mode"
msgstr "ترجیحات کاربراتور در حالت تیره"

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:70
msgid "Updated to the GNOME 47 platform."
msgstr "به‌روز شده به بن‌سازهٔ گنوم ۴۷."

#: data/metainfo/io.frama.tractor.carburetor.metainfo.xml.in:71
msgid "Updated translations."
msgstr "ترجمه‌های به‌روز شده."

#: data/gtk/help-overlay.ui:28
msgctxt "ShortcutsGroup title"
msgid "General"
msgstr "عمومی"

#: data/gtk/help-overlay.ui:32
msgctxt "shortcut"
msgid "Preferences"
msgstr "ترجیحات"

#: data/gtk/help-overlay.ui:38
msgctxt "shortcut"
msgid "Show Shortcuts"
msgstr "نمایش میان‌برها"

#: data/gtk/help-overlay.ui:44
msgctxt "shortcut"
msgid "Quit"
msgstr "خروج"

#: data/gtk/help-overlay.ui:51
msgctxt "ShortcutsGroup title"
msgid "Functionality"
msgstr "کاربردی"

#: data/gtk/help-overlay.ui:55
msgctxt "shortcut"
msgid "Connect / Disconnect"
msgstr "قطع یا وصل شدن"

#: data/gtk/help-overlay.ui:61
msgctxt "shortcut"
msgid "New ID"
msgstr "هویت جدید"

#: data/gtk/help-overlay.ui:68
msgctxt "ShortcutsGroup title"
msgid "Logs"
msgstr "گزارش‌ها"

#: data/gtk/help-overlay.ui:72
msgctxt "shortcut"
msgid "Clear Logs"
msgstr "پاک‌سازی گزارش‌ها"

#: data/gtk/help-overlay.ui:78
msgctxt "shortcut"
msgid "Copy Logs"
msgstr "رونوشت از گزارش‌ها"

#: data/gtk/logs.ui:32
msgid "Logs"
msgstr "گزارش‌ها"

#: data/gtk/logs.ui:39
msgctxt "tooltip"
msgid "Clear"
msgstr "پاک‌سازی"

#: data/gtk/logs.ui:46
msgctxt "tooltip"
msgid "Copy"
msgstr "رونوشت"

#: data/gtk/main.ui:56
msgctxt "Button tooltip"
msgid "Logs"
msgstr "گزارش‌ها"

#: data/gtk/main.ui:63
msgctxt "Button tooltip"
msgid "New ID"
msgstr "هویت جدید"

#: data/gtk/main.ui:70
msgctxt "Button tooltip"
msgid "Check connection"
msgstr "بررسی اتّصال"

#: data/gtk/main.ui:80
msgctxt "tooltip"
msgid "Main Menu"
msgstr "فهرست اصلی"

#: data/gtk/main.ui:95 src/ui.py:123
msgid "_Connect"
msgstr "_وصل شدن"

#: data/gtk/main.ui:99 src/ui.py:121
msgid "Press the \"Connect\" button to start"
msgstr "زدن دکمهٔ «وصل شدن» برای آغاز"

#: data/gtk/main.ui:101 src/ui.py:120
msgid "Stopped"
msgstr "متوقّف"

#: data/gtk/main.ui:120
msgid "_Set Proxy"
msgstr "تنظیم پیشکار"

#: data/gtk/main.ui:126
msgctxt "App Menu Item"
msgid "_Preferences"
msgstr "_ترجیحات"

#: data/gtk/main.ui:130
msgctxt "App Menu Item"
msgid "_Keyboard Shortcuts"
msgstr "_میان‌برهای صفحه‌کلید"

#: data/gtk/main.ui:134
msgctxt "App Menu Item"
msgid "_About Carburetor"
msgstr "_دربارهٔ کاربراتور"

#: data/gtk/preferences.ui:26
msgctxt "PreferencesPage title"
msgid "General"
msgstr "عمومی"

#: data/gtk/preferences.ui:29
msgctxt "PreferencesGroup title"
msgid "Exit Node"
msgstr "گره خروجی"

#: data/gtk/preferences.ui:33
msgid "The country you want to connect from"
msgstr "کشوری که می‌خواهید از آن وصل شوید"

#: data/gtk/preferences.ui:34
msgid "Exit Country"
msgstr "کشور خروجی"

#: data/gtk/preferences.ui:44
msgid "Country Code"
msgstr "کد کشور"

#: data/gtk/preferences.ui:52
msgctxt "PreferencesGroup title"
msgid "Connections"
msgstr "اتّصال‌ها"

#: data/gtk/preferences.ui:55
msgid "Allow external devices to use this network"
msgstr "اجازه به افزاره‌های خارجی برای استفاده از این شبکه"

#: data/gtk/preferences.ui:56
msgid "Accept Incoming Connections"
msgstr "پذیرش اتّصال‌های ورودی"

#: data/gtk/preferences.ui:62
msgid "Restrict connections to ports 80 and 443"
msgstr "محدود کردن اتّصال‌ها به درگاه‌های ۸۰ و ۴۴۳"

#: data/gtk/preferences.ui:63
msgid "Enable Fascist Firewall Mode"
msgstr "به کار انداختن حالت دیوارهٔ آتش فاشیست"

#: data/gtk/preferences.ui:74
msgctxt "PreferencesPage title"
msgid "Proxy"
msgstr "پیشکار"

#: data/gtk/preferences.ui:77
msgid "Ports on which the application listens for incoming connections"
msgstr "درگاه‌های شنود برنامه برای اتّصالات ورودی"

#: data/gtk/preferences.ui:78
msgid "Listening Ports"
msgstr "درگاه‌های شنود"

#: data/gtk/preferences.ui:82
msgctxt "Preferences SpinRow subtitle"
msgid "Main connection point for secure communication"
msgstr "نقطهٔ اتّصال اصلی برای ارتباط امن"

#: data/gtk/preferences.ui:83
msgctxt "Preferences SpinRow title"
msgid "SOCKS"
msgstr "ساکس"

#: data/gtk/preferences.ui:90
msgctxt "Preferences SpinRow subtitle"
msgid "A local DNS server for enhanced privacy"
msgstr "کارساز ساناد محلی برای محرمانگی بیش‌تر"

#: data/gtk/preferences.ui:91
msgctxt "Preferences SpinRow title"
msgid "DNS"
msgstr "ساناد"

#: data/gtk/preferences.ui:98
msgctxt "Preferences SpinRow subtitle"
msgid "A fallback HTTP Tunnel for simple connections"
msgstr "تونل HTTP جایگزین برای اتّصال‌های ساده"

#: data/gtk/preferences.ui:99
msgctxt "Preferences SpinRow title"
msgid "HTTP"
msgstr "وب"

#: data/gtk/preferences.ui:110
msgctxt "PreferencesPage title"
msgid "Bridges"
msgstr "پل‌ها"

#: data/gtk/preferences.ui:113
msgctxt "PreferencesGroup title"
msgid "Pluggable Transports"
msgstr "جابه‌جاگرهای افزایشی"

#: data/gtk/preferences.ui:117
msgid "Type of Transport"
msgstr "گونهٔ جابه‌جاگر"

#: data/gtk/preferences.ui:124
msgid "Transport Executable File"
msgstr "پروندهٔ اجرایی جابه‌جاگر"

#: data/gtk/preferences.ui:129 src/handler.py:244 src/ui.py:226
msgid "None"
msgstr "هیچ‌کدام"

#: data/gtk/preferences.ui:141
msgid ""
"Bridges help you securely access the Tor Network in places where Tor is "
"blocked"
msgstr "پل‌ها در وصل شدن امن به شبکهٔ تور در جاهایی که مسدود است کمکتان می‌کنند"

#: data/gtk/preferences.ui:142
msgctxt "PreferencesGroup title"
msgid "Bridges"
msgstr "پل‌ها"

#: data/gtk/preferences.ui:163 src/handler.py:369
msgid "_Add"
msgstr "_افزودن"

#: data/gtk/preferences.ui:175
msgid "View as a text file to edit and share bridges"
msgstr "دیدن به شکل پرونده‌ای متنی برای ویرایش و هم‌رسانی پل‌ها"

#: data/gtk/preferences.ui:176
msgid "_Open Externally"
msgstr "_گشودن خارجی"

#: data/gtk/preferences.ui:189
msgid ""
"Since many bridge addresses aren’t public, you may need to request some from "
"the Tor Project"
msgstr ""
"از آن‌جا که بسیاری از پل‌ها عمومی نیستند، شاید لازم باشد چند پل از پروژهٔ تور "
"درخواست کنید"

#: data/gtk/preferences.ui:190
msgctxt "PreferencesGroup title"
msgid "Find More Bridges"
msgstr "یافتن پل‌های بیش‌تر"

#: data/gtk/preferences.ui:195
msgid "Visit BridgeDB website"
msgstr "سر زدن به پایگاه وب BridgeDB"

#: data/gtk/preferences.ui:196
msgid "Web"
msgstr "وب"

#: data/gtk/preferences.ui:208
msgid "Exclusively from Gmail or Riseup"
msgstr "فقط از جی‌میل یا رایزآپ"

#: data/gtk/preferences.ui:209
msgid "Email"
msgstr "رایانامه"

#: data/gtk/preferences.ui:222
msgid "Telegram"
msgstr "تلگرام"

#: src/actions.py:95
msgid "translator-credits"
msgstr "دانیال بهزادی <dani.behzi@ubuntu.com>"

#: src/actions.py:117
msgid "_Cancel"
msgstr "_لغو"

#: src/actions.py:127
msgid "Disconnecting…"
msgstr "در حال قطع شدن…"

#: src/actions.py:131
msgid "Connecting…"
msgstr "در حال وصل شدن…"

#: src/actions.py:168
msgid "Text copied to clipboard"
msgstr "متن در تخته‌گیره رونوشت شد"

#: src/actions.py:179
msgid "You have a new identity!"
msgstr "یک هویت جدید دارید!"

#: src/actions.py:181
msgid "Tractor is not running!"
msgstr "تراکتور در حال اجرا نیست!"

#: src/actions.py:211
msgid "Proxy has been set"
msgstr "پیشکار تنظیم شد"

#: src/actions.py:214
msgid "Proxy has been unset"
msgstr "پیشکار برداشته شد"

#: src/actions.py:291
msgid "No relevant bridges found"
msgstr "هیچ پل مربوطی پیدا نشد"

#: src/actions.py:306
msgid "Transport executable not found"
msgstr "پروندهٔ اجرایی جابه‌جاگر پیدا نشد"

#: src/actions.py:347
msgid "One of the listener ports is in use"
msgstr "یکی از درگاه‌های شنود در حال استفاده است"

#: src/actions.py:383
msgid "Your connection is secure"
msgstr "ارتباطتان امن است"

#: src/actions.py:386
msgid "Failed to connect to TOR network"
msgstr "شکست در وصل شدن به شبکهٔ تور"

#: src/actions.py:394
msgid "Connection check complete"
msgstr "بررسی ارتباط کامل شد"

#: src/actions.py:398
msgid "Try again"
msgstr "تلاش دوباره"

#: src/actions.py:399
msgid "Quit"
msgstr "خروج"

#: src/handler.py:105 src/handler.py:140 src/handler.py:159
msgid "Other (Manual)"
msgstr "دیگر (دستی)"

#: src/handler.py:135
msgid "Auto (Best)"
msgstr "خودکار (بهترین)"

#: src/handler.py:245 src/handler.py:306
msgid "Vanilla"
msgstr "وانیلی"

#: src/handler.py:246 src/handler.py:315
msgid "Obfuscated"
msgstr "مبهم شده"

#: src/handler.py:247 src/handler.py:317
msgid "Snowflake"
msgstr "دانه برفی"

#: src/handler.py:248 src/handler.py:319
msgid "Conjure"
msgstr "تداعی"

#: src/handler.py:328
msgid "Connected"
msgstr "وصل شده"

#: src/handler.py:347
msgid "Remove"
msgstr "برداشتن"

#: src/handler.py:418
msgid "Duplicate"
msgstr "تکراری"

#: src/ui.py:146
msgid "Running"
msgstr "در حال اجرا"

#: src/ui.py:147
msgid "Socks Port"
msgstr "درگاه ساکس"

#: src/ui.py:148
msgid "DNS Port"
msgstr "درگاه ساناد"

#: src/ui.py:149
msgid "HTTP Port"
msgstr "درگاه وب"

#: src/ui.py:160
msgid "_Disconnect"
msgstr "_قطع شدن"

#: src/ui.py:178
msgid "Carburetor is connected"
msgstr "کاربراتور وصل است"

#: src/ui.py:180
msgid "Proxy has been set too"
msgstr "پیشکار نیز تنظیم شد"

#: src/ui.py:183
msgid "You may want to use the ports or toggle the proxy now"
msgstr "اکنون می‌توانید از درگاه‌ها استفاده کرده یا وضعیت پیشکار را تغییر دهید"

#: src/ui.py:185
msgid "Toggle Proxy"
msgstr "تغییر وضعیت پیشکار"

#~ msgid "Gmail or Riseup"
#~ msgstr "جی‌میل یا رایزآپ"

#~ msgctxt "App Menu Item"
#~ msgid "_Quit"
#~ msgstr "_خروج"

#~ msgctxt "tooltip"
#~ msgid "More Options"
#~ msgstr "گزینه‌های بیش‌تر"

#~ msgctxt "Action Menu Item"
#~ msgid "_New ID"
#~ msgstr "_هویت جدید"

#~ msgctxt "Action Menu Item"
#~ msgid "_Toggle proxy on system"
#~ msgstr "_تغییر وضعیت پیشکار روی سامانه"

#~ msgid "Options"
#~ msgstr "گزینه‌ها"

#~ msgid "Automatically set proxy after successful connection"
#~ msgstr "تنظیم خودکار پیشکار پس از اتّصال موفّق"

#~ msgctxt "PreferencesPage title"
#~ msgid "Ports"
#~ msgstr "درگاه‌ها"

#~ msgid "WebTunnel"
#~ msgstr "تونل وب"

#~ msgid "Make the Main window less empty on first openning"
#~ msgstr "پُرتر کردن پنجرهٔ اصلی در نخستین گشودن"

#~ msgid "Prevent starting if there is no relevant bridge or transport"
#~ msgstr "پرهیز از آغاز در صورت نبودن پل یا جابه‌جاگر مرتبط"

#~ msgid "Add option to set manual country code for exit node"
#~ msgstr "افزودن گزینهٔ تنظیم دستی کد کشور برای گره خروجی"

#~ msgid "Add <em>What's New</em> section to About Dialog"
#~ msgstr "افزودن بخش <em>چیزهای جدید</em> به گفت‌وگوی درباره"

#~ msgid "Let the window to be closed after canceling the connection"
#~ msgstr "اجازه به بسته شدن پنجره پس از لغو اتّصال"

#~ msgid "Use notifications for tasks if the window is not focused"
#~ msgstr "استفاده از آگاهی‌ها برای وظایف در صورت تمرکز نداشتن پنجره"

#~ msgid "_Add Bridge"
#~ msgstr "_افزودن پل"

#~ msgid "Please copy and paste the bridges you have in the area below."
#~ msgstr "رونوشت و جایگذاری پل‌هایتان در ناحیهٔ زیر."

#~ msgid "_Save"
#~ msgstr "_ذخیره"

#~ msgid "Save Bridges as a file in your local configs"
#~ msgstr "ذخیرهٔ پل‌ها به شکل یک پرونده در پیکربندی‌های شخصیتان"

#~ msgid "_OK"
#~ msgstr "_قبول"

#~ msgid "Remove started/stopped toasts"
#~ msgstr "برداشتن برنماهای آغاز و توقّف"

#~ msgid "Improve some strings inside the app"
#~ msgstr "بهبود برخی رشته‌های درون کاره"

#~ msgid "Port to Meson build system"
#~ msgstr "انتقال به سامانهٔ ساخت مزون"

#~ msgid ""
#~ "To Get bridges, you have these options:\n"
#~ "\n"
#~ "• Visit the Tor website: <a href=\"https://bridges.torproject.org/options/"
#~ "\">https://bridges.torproject.org/options</a>\n"
#~ "• Use the Tor bridges Telegram bot: <a href=\"https://t.me/"
#~ "GetBridgesBot\">Tor bridges Telegram bot</a>\n"
#~ "• Send an email from a Riseup or Gmail address to: <a href=\"mailto:"
#~ "bridges@torproject.org?body=get bridges\">bridges@torproject.org</a>"
#~ msgstr ""
#~ "گزینه‌های زیر برای گرفتن پل‌ها وجود دارند:\n"
#~ "\n"
#~ "• سر زدن به پایگاه وب تور: <a href=\"https://bridges.torproject.org/"
#~ "options/\">https://bridges.torproject.org/options</a>\n"
#~ "• استفاده از روبوت تلگرامی پل‌های تور: <a href=\"https://t.me/"
#~ "GetBridgesBot\">روبوت تلگرامی پل‌های تور</a>\n"
#~ "• فرستادن رایانامه از نشانی رایزآپ یا جی‌میل به <a href=\"mailto:"
#~ "bridges@torproject.org?body=get bridges\">bridges@torproject.org</a>"

#~ msgid ""
#~ "Please check the bridges to match the selected pluggable transport type."
#~ msgstr "لطفاً پل‌ها را برای مطابقت با گونهٔ جابه‌جاگر افزایشی گزیده بررسی کنید."

#~ msgid "_Logs"
#~ msgstr "_گزارش‌ها"

#~ msgid "Austria"
#~ msgstr "اتریش"

#~ msgid "Bulgaria"
#~ msgstr "بلغارستان"

#~ msgid "Canada"
#~ msgstr "کانادا"

#~ msgid "Switzerland"
#~ msgstr "سوییس"

#~ msgid "Czech"
#~ msgstr "چک"

#~ msgid "Germany"
#~ msgstr "آلمان"

#~ msgid "Spain"
#~ msgstr "اسپانیا"

#~ msgid "Finland"
#~ msgstr "فنلاند"

#~ msgid "France"
#~ msgstr "فرانسه"

#~ msgid "Ireland"
#~ msgstr "ایرلند"

#~ msgid "Moldova"
#~ msgstr "مولداوی"

#~ msgid "Netherlands"
#~ msgstr "هلند"

#~ msgid "Norway"
#~ msgstr "نروژ"

#~ msgid "Poland"
#~ msgstr "لهستان"

#~ msgid "Romania"
#~ msgstr "رومانی"

#~ msgid "Seychelles"
#~ msgstr "سیشل"

#~ msgid "Sweden"
#~ msgstr "سوئد"

#~ msgid "Singapore"
#~ msgstr "سنگاپور"

#~ msgid "Russia"
#~ msgstr "روسیه"

#~ msgid "Ukraine"
#~ msgstr "اوکراین"

#~ msgid "United Kingdom"
#~ msgstr "بریتانیا"

#~ msgid "United States"
#~ msgstr "آمریکا"

#~ msgid "Tractor couldn't connect"
#~ msgstr "تراکتور نتوانست وصل شود"

#~ msgid "Tractor is stopped"
#~ msgstr "تراکتور متوقّف شده است"

#~ msgid "Tractor is running"
#~ msgstr "تراکتور در حال اجراست"

#~ msgctxt "tooltip"
#~ msgid "Show Details"
#~ msgstr "نمایش جزییات"

#~ msgid "Local port on which Tractor would be listen"
#~ msgstr "درگاه محلّی برای شنود تراکتور"

#~ msgid "Local port on which you would have an anonymous name server"
#~ msgstr "درگاه محلّی برای ساناد ناشناس"

#~ msgid "Local port on which a HTTP tunnel would be listen"
#~ msgstr "درگاه محلّی برای شنود یک تونل HTTP"

#~ msgid "Can not save the bridges"
#~ msgstr "نمی‌توان پل‌ها را ذخیره کرد"

#~ msgid "Please check the syntax of bridges"
#~ msgstr "لطفاً ترکیب پل‌ها را بررسی کنید"

#~ msgid "Type"
#~ msgstr "گونه"

#~ msgid "Type of bridge"
#~ msgstr "گونهٔ پل"

#~ msgid "Client Transport Plugin"
#~ msgstr "افزایهٔ جابه‌جایی کارخواه"

#~ msgid "GTK frontend for Tractor"
#~ msgstr "پیشانهٔ GTK برای تراکتور"

#~ msgid "Copyright © 2019-2023, Tractor Team"
#~ msgstr "حق رونوشت © ۱۳۹۸-۱۴۰۲، تیم تراکتور"

#~ msgid "Copyright © 2019-2022, Tractor Team"
#~ msgstr "حق رونوشت © ۱۳۹۸-۱۴۰۲، تیم تراکتور"

#~ msgid "none"
#~ msgstr "هیچ‌کدام"

#~ msgid "vanilla"
#~ msgstr "وانیلی"

#~ msgid "obfuscated"
#~ msgstr "مبهم شده"

#~ msgid "Use obfs4proxy"
#~ msgstr "استفاده از obfs4proxy"

#~ msgid "Please specify the plugin executable file"
#~ msgstr "لطفاً پروندهٔ اجرایی افزایه را مشخّص کنید"

#~ msgid "Start"
#~ msgstr "آغاز"

#~ msgid "Stop"
#~ msgstr "توقّف"

#~ msgid "stopping…"
#~ msgstr "در حال توقّف…"

#~ msgid "starting…"
#~ msgstr "در حال آغاز…"

#~ msgid "<b>Use bridges</b>"
#~ msgstr "<b>استفاده از پل‌ها</b>"

#~ msgid "<small>Bridges help you to bypass tor sensorship</small>"
#~ msgstr "<small>پل‌ها به دور زدن سانسور کمک می‌کنند</small>"

#~ msgid "Bridges:"
#~ msgstr "پل‌ها:"

#~ msgid "<b>obfs4proxy executable</b>"
#~ msgstr "<b>پروندهٔ اجرایی obfs4proxy</b>"

#~ msgid "<small>You should specify where is obfs4proxy file</small>"
#~ msgstr "<small>باید مشخّص کنید پروندهٔ obfs4proxy کجاست</small>"
